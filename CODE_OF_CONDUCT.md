# Code of Conduct 

The following rules and regulations are applied till end of the project

## Communication Rules
The following rules apply to all means of communication, online and in real life.
- Do not use foul language, and absolutely NO insults will be tolerated.
- Do not do direct criticism:
    - Focus on the possible improvements and do not make the issue a personal one.
    - You should, when possible, criticize in private, and praise publicly
- Do not troll people; it is not funny.
- Do not undo someone's else contribution (wiki edits, code reverts, etc...) without asking the permission from them.
- Use English for most of the communication 
- When talking to Project people, it is allowed to use differnet language
- Do not spam in IRC channel "#iris"
- Do not abuse admin powers 

These Code of Conduct rules are not exhaustive; please use common sense, to infer other rules.

## Daily Updates
The following rules are enfocred for daily updates
- The project members have to send daily status update without fail, even if they did not work
- If a person fails to send status update for 2 consecutive days, he/she will be having temporary ban and some of action will be taken on him depending on the project lead.
- You should not quote any person's quote without asking them
- One should not create a new thread everyday, instead they can use the same thread for atleast 3 months/100 Mails whichever is earlier.
- One should respond when they are asked to do something by Project Advisor/Mentor/Lead. Failure to respond within 24 hours will again result in some action.

## IRC 
When using the #iris IRC channel, please follow these rules:
- Do not quote messages that were sent to you by other people in private query, unless agreed beforehand.
- If you want to explain to someone how to use IRC, do it privately.
- Never send messages with colours (i.e. colour codes); use plain text instead.
- Do not send "test" messages to determine whether your IRC client is working. We have a #test channel for that.

## Code repositories: Gitlab
When you are using GitLab instance of the project, following rules are applied
- Do NOT engage in revert wars.
- Do NOT delete another developer's commit without approval by using push --force.
- No insult in commits or commit messages.
- Respect the [correct commit log style](http://chris.beams.io/posts/git-commit/).
- Do not push to master branch unless it is approved from all project collaborators
- Do not create multiple branches, ask the project lead for getting permission to create new branch 
- When you would like to have new changes to be in master, please do a pull-request then have multiple reviews then it can be merged in master.


## Disciplinary actions
Voilations of Code of Conduct will be punished,
See the following guidelines for escalations steps and sanctions.

### Escalation
- The first violation will always result in a simple warning, except if it is a grave or deliberate violation.
- The following violations will result in some of the disciplinary actions listed in the paragraphs below.
- For repetitive violations, the case may be escalated to the Project Advisor for further disciplinary actions.

In case of any disciplinary action, The Project Advisor will send an email to the offender and copy the mail to Project Mentor, Project Lead and also to offender.

## Disciplinary Actions

The following disciplinary actions may be enforced when a direct Code of Conduct violation is reported.

NB: Before applying any of those following disciplinary policies, the Project Mentor will try to discuss the problem with the offender in order to solve it in a more peaceful way.
However, it is possible for the Project Advisor to apply the penalty without discussions in severe Code of Conduct violations.

### General Disciplinary Actions
- The netiquette violations will get only a warning. Repeated netiquette violations will be escalated.
- Every third violation, the contributor will get a 7-day ban.
- One day ban from a GitLab Instance with ban of commit access.
- For spam violations, the first ban can be longer or infinite.
- Direct kick for minor violations, enforced by Project Lead. Other violations will get a 24-hour ban from the IRC channel.
- Removal of Developer or admin rights
- Every third violation, the developer will lose commit access for 7 days or removal of them from the project.

This Code Of Conduct is made in accordance with  [VideoLAN](https://wiki.videolan.org/CoC) Policies and also with [Mozilla General Guidlines](https://github.com/mozilla/diversity/blob/master/evaluation_tools/governance-basic.md)