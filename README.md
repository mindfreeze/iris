## Project Iris Website

The Canonical URL for the website is: https://mindfreeze.videolan.me/iris/


![Imgur](https://i.imgur.com/exQ0syj.png)


## Steps to host it locally 

1.  Clone this repo `https://code.videolan.org/mindfreeze/iris.git`
2.  Install jekyll if not available 
3.  `bundle exec jekyll build --destination ./public`
4.  `bundle exec jekyll serve`


A Jekyll theme based on [Grayscale bootstrap theme ](http://ironsummitmedia.github.io/startbootstrap-grayscale/)