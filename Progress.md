
# Month Wise  Report of the Project Progress

## January 2019

We started thinking about the different fields in which all the team members were equally interested in doing the final year project. We had a common interest in solving real-life problems which are not explored much till now. So we thought doing something with Video Technology will be nice.

We chose this topic as it is a field which we believe requires very much attention in this technologically advanced era, where there exists no proper standard solution for real-time video streaming from low power lot devices. 

In January we had fixed the domain and drafted the project goal and had a discussion with you about doability and you had advised us to get the fundamentals ready.

## February 2019

Since, we didn't have a background in this field, we discussed the scope of the project to different people in the videoo industry. So we can to the conclusion that many device manufacturers who have exactly this problem, they want a low power (e.g., low complexity) way of streaming video that is not huge.
They have cleared the doubts we had in the initial project roadmap and also helped us in getting the basic understanding. We also finalised that we shall work parallely with both the encoders and have a comparison of both of them throughout the whole process. For the testing purpose, we initially decided to use the NXP.IMX.7D board since it was already available in hand.

x265 have inbuilt tool for generating the metadata in CSV format. Initially, we thought of building a common parser which could be given output to a CSV and plot some graphs like make a similar tool in rav1e too. So in the end, both x265 and rav1e have more or less the same metadata for doing comparisons.

Since we cannot always rely on board so we had to do cross-compiling of the encoder for armv7 arch. We have used Dummy data provided by the rav1e team for testing.

Vibhoothi have published a Blog [Understanding more about AOM Analyzer](https://mindfreeze.videolan.me/why-you-should-care-about-aom-analyzer-tool-e2-80-8b/).
Also, he have started to attend [Daala Weekly Meetings over Mumble](https://public.etherpad-mozilla.org/p/daala-weekly-meeting), here you can see the minutes of the meeting.

## March 2019
By the first week of March, we were successfully able to build locally and we did some encoding by enabling and disabling various features like Quantization Parameter, Constant Rate Factor, Loop restoration, PSNR and much more. All of us then spend a considerable amount of time understanding the theory of Video Technology and encoding like Quantizations, Transformations and much more based on [this](https://github.com/leandromoreira/digital_video_introduction). Secondly, we also spend time understanding and learning rust as rav1e is completely written in rust. So in favour of learning, Vibhoothi have tried to add a new [feature](https://github.com/xiph/rav1e/commit/8093e4eeeb5fb0e53ed99c131a72734a5be08806) to the rav1e encoder to skip n number of frames and then do the encoding.

During the Second week and third week of march, we couldn't focus much on the project due to Vidyut, the Annual Tech Fest of the college and also due to Internal Examination of University.

Later that month Vibhoothi, had started doing documentation to rav1e's [api.rs](https://github.com/xiph/rav1e/projects/8) and submitted couple of PRs. 

Later in March Vaishnav, lead of x265 Team had studied in depth about Structural Similarity Index ([SSIM](http://www.imatest.com/docs/ssim/)) to check video quality. After that, he have tried to do some hacking in Constant Rate Factor, bitrates value to get the matching size of the video of both encoders. 

Also by this time, we have been trying to do cross-compiling with the required toolchain. The rav1e team have continued studying of AV1 Bitstream Documentation. Investigated the decoding inspector in the libaom decoder.

For making the stream analyzer, we thought of using [Azul](https://azul.rs/), a GUI framework with rust for stream analyzer and then made some plans, it was a difficult problem to solve since there is no working solution right now, we need to get started by building a more strong foundation like making a proper platform for future development which is currently not available. For visualising, earlier we thought to do similar to AOM Analyzer. 

Also in watercooler meeting of rav1e, and vibhoothi had a discussion about the tool and project with other developers and finalised that this proposed tool will be focusing on Decoder Analyzer based on reference encoder's decoder(libaom). So once the platform is ready, we will be expanding the tool with the addition of encoder analyzer with encoder metadata from rav1e. In first proposed encoder analyzer, we will be taking into account of various parameters like Trade-off between speed and quality, Motion Search, Motion Vectors, Block structures, and much more which could potentially improve the production quality of the encoder. For supporting the rav1e, we would require a stable API where this solution is also written in rust itself with the help of Azul Framework.

Also in the last week of March had a discussion with Azul team about the issues with macOS of Azul framework and tried to fix it.

## April 2019

By the First week of April, Vibhoothi have completed the final draft of the [proposed tool](https://docs.google.com/document/d/1gIuC3i347y9gJ7KgXOUCOKbo8x0jfOPSKs-JwU-zA3U/edit?ouid=108006637179298699879&usp=docs_home&ths=true), It is Titled as ARA,

"**ARA**: **ARA** is a **r**av1e **A**nalyzer".

Also, work of documentation of APIs is on progress. 

We tried to do the cross compilation, for rav1e, the cross compiler isn't available as it last updated in 2009, nearly 10 years old, we have made a updated version of toolchain with [MacPorts](https://github.com/vibhoothiiaanand/MacPorts) and eventually we were successful in cross-compiling rav1e in macOS. 

While x265, we were unable to run the binary after cross-compiling due to the mismatched cross-compiling tool and lack of native support of cross-compiling. It took some amount of time to complete the cross-compiling and different hacks with cross compiling x265. 

At this time we tried to boot up ubuntu in NXP Board and we had a late realisation that it is nearly impossible to boot up ubuntu in that since we have to do some hardware hacks to change the kernel and OS from Android Things to other. So we indefinitely stopped using NXP board and added buying RPI to the to-do list. 

We also managed to get some scripts to play which written by EwoutH for [rav1e](https://pastebin.com/raw/UHXzLT38), [SVT-AV1](https://pastebin.com/raw/FkzM3fnR), [dav1d](https://pastebin.com/raw/SAjKn6Q9). We did the same and tried to test it locally. 

During last week of April, we didn't have much time to do the project as we had multiple course-work projects, assignments to do. However, we also did spend time reading research papers related to Video Technology and was active in the community for development and discussions. 

Srijith have been trying to work with some 8k RAW Footage from RED Cameras which is in R3D format, tried and investigated REDCINE-X Pro for decoding the r3d files, also spend some time trying to figure out how we could have r3d transcoder so we can use this 8k footage to encode using x265 and rav1e. Also had a discussion with FFMpeg team to have this but they were not much interested to have this into the codebase since it is proprietary and not open source.

## May 2019

First *3 weeks* of May, we couldn't work due to finals, in the last week of May.

We did something interesting things after finals, ie. we have encoded the same video with both rav1e and x265 and we had plotted [some graphs with it](https://docs.google.com/spreadsheets/d/1gdhlgVfiscyXoaEFjSisoYE-ta15VY1gVd3QcXuNNFw/edit#gid=0).

Also made a [landing page](https://mindfreeze.videolan.me/iris/) for the project and hosted it. Te source code is available in [https://code.videolan.org](https://code.videolan.org/mindfreeze/iris).

## June 2019 [Ongoing...]

The first week of June was again working with analyzing the encoding and plotting different graphs and we did do some graphs. We had kept multiple internal meetings doing discussions and revamping of the roadmaps ahead.

Since x265 didn't have an option of giving details regarding bitrate of each frame while encoding an input video, Vaishnav have [worked](https://github.com/vaishnavsivadas/Change-File/blob/master/x265.cpp) and made it possible to get bitrate of frame-wise like we get in rav1e. This helped us to plot a graph of both the encoders and have a good analysis of the result at hand. In the meeting, we have discussed and finalised that we will be using either Raspberry Pi Model B/B+ and we brought B+. 

For rav1e, the main thing we are going to focus is having ARM NEON SIMD Optimisations and having full ARM support to rav1e. For that first step, we would require to have proper support of assembler in the build system. Currently, [nasm-rs](https://github.com/medek/nasm-rs) is used for x86 targets and vibhoothi have made nasm-rs as a (subcrate)[https://github.com/xiph/rav1e/commit/1b4d72a41236788d95ad7ce02d5c5cfee6806c57] instead of seperate one. For armv7/aarch64 assembly programs, we will be using [cc-rs](https://github.com/alexcrichton/cc-rs) which will be in GAS (GNU Assembler). Currently, rav1e has only [support for NASM syntax](https://github.com/xiph/rav1e/issues/1276).

We are also investigating [dav1d](https://code.videolan.org/videolan/dav1d) about the feasibility of taking which all to rav1e. 

So for profiling time spend on each stage, we have used [perf](https://linux.die.net/man/1/perf-record) and also [uftrace](https://github.com/namhyung/uftrace) to find the hotspots. And for these hotspots we will be trying to have NEON SIMD in coming months, that will give around very significant increase in Encoding Speed.

Currently, we are trying to understand about [VMAF](https://github.com/Netflix/vmaf) from Netflix and also about [AWCY](https://beta.arewecompressedyet.com/) use cases.

Also Srijith is exploring the possibility of [LoRa](https://www.semtech.com/lora/what-is-lora) in this project.
From some research on how to actually achieve our goal in x265, we found out that in HEVC encoders, intra-prediction is a complex process and from some reference papers, we found out that the encoder complexity, as well as the video encoding quality loss, could be reduced by implementing an efficient learning based approach. Coming forward, we later came across a paper which reports that HEVC encoding standard has already adopted some of the aforementioned approaches such as [Logistic Regression](https://ieeexplore.ieee.org/document/7906399), [decision trees](https://www.researchgate.net/publication/316603193_Fast_Intra_Coding_Algorithm_for_HEVC_Based_on_Decision_Tree), [random forest](https://ieeexplore.ieee.org/document/8698675) and [Bayesian Classification](https://ieeexplore.ieee.org/document/7552970) etc. We believe that implementing any such technique in x265, as an actual start towards achieving our goal would be a good thing.  A neon optimization technique for arm was also found to be an option during the learning phase and we've been looking into that as well. Since the NEON technology revolves around the optimization/improvement within the code, we plan to use it later as needed after the successful implementation of a good approach for improving the encoding process in whole.

We were also successful in building rav1e and x265 in the board, and we also did some encoding in it.

We will be running the tests in AWCY

So later this month we will be spending time on moving from learning phase to the development phase of the actual problem.

