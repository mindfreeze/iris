# Weekly status report

---

## Week of Jun 17

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
1. Created a Detailed [Month wise report](https://gitlab.com/vibhoothiiaanand/iris/blob/master/Progress.md).
2. We also have a sheet with some random [graphs](https://gitlab.com/vibhoothiiaanand/iris/blob/master/Progress.md).
3. Discussed Internally about the future roaadmaps.
4. Made nasm-rs a sub-crate inside rav1e main repo and is merged into the [master](https://github.com/xiph/rav1e/commit/1b4d72a41236788d95ad7ce02d5c5cfee6806c57).

### Issues/Concerns/Road blocks
1. Nothing Techinically 

### Action plan to address issues
1. NA

### Plans for the next week
1. Start learning Armhf assembly 

### Comments from guide (very specific)
  
---

## Week of Jun 24

### Accomplishments of the week
1. Learning Arm Assembly from [Think Geek](https://thinkingeek.com/arm-assembler-raspberry-pi/). All the members have completed 7 chapters from the tutorial and some development from Vibhoothi is pushed in [GitHub](https://github.com/vibhoothiiaanand/as_playground).
2. Build x265 with asm in board itself and tried to encode with asm enabled and we found that there
was a signicant increase in the encoding speed when we enabled the assembly. 

### Issues/Concerns/Road blocks
1. When using rpi over internet, ping is very high
2. Learning ASM is bit slow progress

### Action plan to address issues
1. Use Qemu Vitual emulator for rpi for virtual testing and then use the same in the
	   boards.
2. Learning is a not very easy tasks, so we will try to spend extra hours to get the things
	   move rapidly.

### Plans for the next week
1. Complete till Matrix Multiplication from the tutorial.
2. Add GAS Assembler support to rav1e.
3. List and check the 32 bit assembly in x265.


### Comments from guide (very specific)

---

## Week of Jul 1
